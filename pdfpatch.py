#!/usr/bin/env python
# -*- coding: latin-1 -*-
"Usage: %s input_dir output_dir"

import sys, os
from string import Template
import subprocess as sp
sys.path.append('ppg/Lib')
import pdffile, pages


toc_template = Template(r'''\documentclass[pdftex,a4paper,twoside]{article}
\usepackage[T1]{fontenc}
\usepackage{times}
\usepackage{afterpage}
\usepackage[none]{hyphenat}
\usepackage[%
  bookmarks=false,
  hyperfootnotes=false,
  colorlinks=true,
  filecolor=black,
  urlcolor=black,
  linkcolor=black,
  citecolor=black,
  pagecolor=black,
  anchorcolor=black]{hyperref}
\usepackage{longtable}
\usepackage[margin=2.5cm]{geometry}
\renewcommand{\rmdefault}{phv}
\setlength{\unitlength}{1cm}
\pagenumbering{roman}
\begin{document}
\setcounter{page}{1}
\par\begin{flushright}\textbf{\Huge Table of Contents}\end{flushright}
\par\rule{0cm}{2cm}
\par\noindent\begin{longtable}{rp{15cm}l}

$content

\end{longtable}
\afterpage{\thispagestyle{empty}}
\cleardoublepage
\end{document}
''')

def run(cmd, env=None, onerror=''):
    proc = sp.Popen(cmd, env=env, shell=True, stdout=sp.PIPE, stderr=open('/dev/null'))
    out = proc.communicate()[0]
    assert proc.returncode == 0, onerror
    return out.strip()


class Pdf:
    def __init__(self, name, path):
        self.path = path
        self.name = name
        self.numpages = self.get_numpages()

    def get_numpages(self):
        doc = pdffile.pdffile(self.path)
        pp = pages.pages(doc)
        return len(pp.pagelist)

    def patch(self, outpath, parche, page):
        cmd = Template("/usr/bin/gs -r600 -dDITHERPPI=120 -dNOCIE -dFIXEDMEDIA " +
                       "-sPAPERSIZE=a4 -swm=$name -dpnr=$beginpage -q -dNOPAUSE " +
                       "-dSAFER -dBATCH -sOutputFile=$output -sDEVICE=pdfwrite " +
                       "pdfopt.ps $pathparche $input")
        
        cmd = cmd.substitute(name=self.name,
                             input=self.path, output=outpath,
                             beginpage=page,
                             pathparche=parche)
        print cmd
        run(cmd)
        

if len(sys.argv) != 3:
    print __doc__ % sys.argv[0]
    sys.exit(1)

in_dir =  sys.argv[1]
out_dir = sys.argv[2]


# obtener la lista de ficheros 
lista = []
for root, dirs, files in os.walk(in_dir):
    for d in dirs:
        if d.startswith('.'):
            dirs.remove(d)
    dirs.sort()

    for f in sorted(files):
        if os.path.splitext(f)[1] != '.pdf': continue
        abspath = os.path.join(root, f)
        lista.append(Pdf(abspath[len(in_dir):], abspath))


prev = 0
for i in lista:
    # calcula p�ginas de inicio
    i.page = prev
    prev += i.numpages

    # aplica el parche
    outname = os.path.join(out_dir, i.name.replace('/', '_'))
    i.patch(outname, 'head.ps', prev)


# genera la TOC
toc = ''
for i in lista:
    print "%-65s %2s %2s" % (i.name, i.page, i.numpages)
    toc += "%-65s & %2s\\\\\n" % (i.name, i.page)

tocfile = open(os.path.join(out_dir, 'tocfile.ltx'), 'w')
tocfile.write(toc_template.substitute(content=toc))
tocfile.close()
